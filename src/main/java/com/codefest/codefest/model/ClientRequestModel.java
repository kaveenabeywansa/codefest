package com.codefest.codefest.model;

public class ClientRequestModel {
	String date;
	String iotId;
	double errorRate;
	double empPerformance;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getIotId() {
		return iotId;
	}
	public void setIotId(String iotId) {
		this.iotId = iotId;
	}
	public double getErrorRate() {
		return errorRate;
	}
	public void setErrorRate(double errorRate) {
		this.errorRate = errorRate;
	}
	public double getEmpPerformance() {
		return empPerformance;
	}
	public void setEmpPerformance(double empPerformance) {
		this.empPerformance = empPerformance;
	}
	
}
