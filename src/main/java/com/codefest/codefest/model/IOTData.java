package com.codefest.codefest.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "IOTData")
public class IOTData {
	@Id
	String iotid;
	String date;
	int totcount;
	int defectcount;

	public String getIotid() {
		return iotid;
	}
	public void setIotid(String iotid) {
		this.iotid = iotid;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getTotcount() {
		return totcount;
	}
	public void setTotcount(int totcount) {
		this.totcount = totcount;
	}
	public int getDefectcount() {
		return defectcount;
	}
	public void setDefectcount(int defectcount) {
		this.defectcount = defectcount;
	}
	
}
