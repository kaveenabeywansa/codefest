package com.codefest.codefest.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="BeltPerSec")
public class BeltPerSec {
	@Id
	private String dtime;
	int itemcount;
	int defectives;
	String iotcode;
	public String getIotcode() {
		return iotcode;
	}
	public void setIotcode(String iotcode) {
		this.iotcode = iotcode;
	}
	public String getDtime() {
		return dtime;
	}
	public void setDtime(String dtime) {
		this.dtime = dtime;
	}
	public int getItemcount() {
		return itemcount;
	}
	public void setItemcount(int itemcount) {
		this.itemcount = itemcount;
	}
	public int getDefectives() {
		return defectives;
	}
	public void setDefectives(int defectives) {
		this.defectives = defectives;
	}
	
}
