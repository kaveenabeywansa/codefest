package com.codefest.codefest.model;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRequestRepository extends MongoRepository<ClientRequestModel, String>{

}
