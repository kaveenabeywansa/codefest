package com.codefest.codefest.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codefest.codefest.model.BeltPerSec;
import com.codefest.codefest.model.BeltRepository;
import com.codefest.codefest.model.IOTData;
import com.codefest.codefest.model.IOTRepository;
import com.codefest.codefest.service.IOTDataServiceImpl;

@CrossOrigin
@RestController
@RequestMapping("/iotdata")
public class IOTDataController {
	@Autowired
	private IOTDataServiceImpl iOtDataService;

	@RequestMapping(method = RequestMethod.POST)
	public void addCycle(@RequestBody IOTData instance) {
		iOtDataService.addCycle(instance);
	}

	@SuppressWarnings("finally")
	@RequestMapping(method = RequestMethod.GET)
	public List<IOTData> getIOTDetails() {
		return iOtDataService.getIOTDetails();
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/{Id}", method = RequestMethod.GET)
	public IOTData getDeviceDetails(@PathVariable String Id) {
		return iOtDataService.getDeviceDetails(Id);
	}

	@RequestMapping(value = "/{Id}", method = RequestMethod.DELETE)
	public void deleteIOTDet(@PathVariable String Id) {
		iOtDataService.deleteIOTDet(Id);
	}

}
