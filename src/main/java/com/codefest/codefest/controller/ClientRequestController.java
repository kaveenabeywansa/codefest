package com.codefest.codefest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codefest.codefest.model.ClientRequestModel;
import com.codefest.codefest.model.IOTData;
import com.codefest.codefest.service.ClientRequestService;

//@CrossOrigin(origins = "http://localhost:8080")
@CrossOrigin
@RestController
@RequestMapping("/clientrequest")
public class ClientRequestController {
	@Autowired
	private ClientRequestService clientRequestService;

//	@RequestMapping(method = RequestMethod.POST)
//	public void addCycle(@RequestBody IOTData instance) {
//		iOtDataService.addCycle(instance);
//	}

//	@SuppressWarnings("finally")
//	@RequestMapping(method = RequestMethod.GET)
//	public List<ClientRequestModel> getIOTDetails() {
//		return clientRequestService.getAllData();
//	}
	
	
	
	//
	@SuppressWarnings("finally")
	@RequestMapping(value = "/error/{Id}", method = RequestMethod.GET)
	public double getDeviceErrorRate(@PathVariable String Id) {
		return clientRequestService.getDeviceErrorRate(Id);
	}
	@SuppressWarnings("finally")
	@RequestMapping(value = "/performance/{Id}", method = RequestMethod.GET)
	public double getDevicePerformance(@PathVariable String Id) {
		return clientRequestService.getDevicePerformance(Id);
	}
	//
	
	

//	@SuppressWarnings("finally")
//	@RequestMapping(value = "/{Id}", method = RequestMethod.GET)
//	public IOTData getDeviceDetails(@PathVariable String Id) {
//		return clientRequestService.getDeviceDetails(Id);
//	}

//	@RequestMapping(value = "/{Id}", method = RequestMethod.DELETE)
//	public void deleteIOTDet(@PathVariable String Id) {
//		iOtDataService.deleteIOTDet(Id);
//	}
}
