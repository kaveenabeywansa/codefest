package com.codefest.codefest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codefest.codefest.model.BeltPerSec;
import com.codefest.codefest.model.BeltRepository;
import com.codefest.codefest.model.Step;
import com.codefest.codefest.model.StepRepository;
import com.codefest.codefest.service.StepServiceImpl;

@CrossOrigin
@RestController
@RequestMapping("/step")
public class StepController {

	@Autowired
	private StepServiceImpl StepServiceImpl;
	
	@RequestMapping(method = RequestMethod.POST)
	public void addStep(@RequestBody Step instance) {
		StepServiceImpl.addStep(instance);
	}
	
	
	@SuppressWarnings("finally")
	@RequestMapping(method = RequestMethod.GET)
	public List<Step> getStepDetails() {
		return StepServiceImpl.getStepDetails();
	}
	
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/{Id}", method = RequestMethod.GET)
	public Step getDeviceDetails(@PathVariable String Id) {
		return StepServiceImpl.getStepDetailsById(Id);
	}
	
	@RequestMapping(value = "/{Id}", method = RequestMethod.DELETE)
	public void deleteStepDet(@PathVariable String Id) {
				StepServiceImpl.deleteStepDetails(Id);
	}
	
	
//	@SuppressWarnings("finally")
//	@RequestMapping(value = "/{Id}", method = RequestMethod.GET)
//	public int updateEmpCount(@PathVariable String Id) {
//		return StepServiceImpl.updateEmpCount(Id);
//	}
	
//	@SuppressWarnings("finally")
//	public Step FindOne(String Id) {
//		return StepServiceImpl.FindOne(Id);
//	}
	
}
