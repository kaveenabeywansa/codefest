package com.codefest.codefest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codefest.codefest.model.EmpStep;
import com.codefest.codefest.model.Step;
import com.codefest.codefest.service.EmpStepServiceImpl;
import com.codefest.codefest.service.StepServiceImpl;

@CrossOrigin
@RestController
@RequestMapping("/empstep")
public class EmpStepController {

	@Autowired
	private EmpStepServiceImpl empStepServiceImpl;
	
	// add emp step to db
	@RequestMapping(method = RequestMethod.POST)
	public void addEmpStep(@RequestBody EmpStep instance) {
		empStepServiceImpl.addEmpStep(instance);
	}
	
	// get all steps details
	@SuppressWarnings("finally")
	@RequestMapping(method = RequestMethod.GET)
	public List<EmpStep> getEmpStepDetails() {
		return empStepServiceImpl.getEmpStepDetails();
	}
	
	// get emps details of an emp
	@SuppressWarnings("finally")
	@RequestMapping(value = "/{Id}", method = RequestMethod.GET)
	public EmpStep getEmpStepDetailsById(@PathVariable String Id) {
		return empStepServiceImpl.getEmpStepDetailsById(Id);
	}
	
	// delete emp using nic
	@RequestMapping(value = "/{Id}", method = RequestMethod.DELETE)
	public void deleteEmpStepDetails(@PathVariable String Id) {
		empStepServiceImpl.deleteEmpStepDetails(Id);
	}
	
//	@SuppressWarnings("finally")
//	@RequestMapping(value = "/{Id}", method = RequestMethod.GET)
//	public Step FindOne(String Id) {
//		return empStepServiceImpl.FindOne(Id);
//	}
	
	// get emps related to an step
	@SuppressWarnings("finally")
	@RequestMapping(value = "/assign/{StepId}", method = RequestMethod.GET)
	public List<EmpStep> FindStepAssignments(@PathVariable String StepId) {
		return empStepServiceImpl.FindStepAssignments(StepId);
	}
	
	// reassigning an emp to a different step
	@SuppressWarnings("finally")
	@RequestMapping(value = "/{nic}", method = RequestMethod.PUT)
	public void updateEmpStepDetails(@PathVariable String nic,@RequestBody EmpStep instance) {
		empStepServiceImpl.updateEmpStepDetails(nic, instance);
	}
	
	@SuppressWarnings("finally")
	@RequestMapping(value = "/getempcount/{StepId}", method = RequestMethod.GET)
	public int getAssignedEmpCount(@PathVariable String StepId) {
		return empStepServiceImpl.getAssignedEmpCount(StepId);
	}
	
}
