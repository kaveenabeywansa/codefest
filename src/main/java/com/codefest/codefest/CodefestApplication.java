package com.codefest.codefest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

//@ComponentScan({"com.codefest.codefest.controller"})
//@ComponentScan({"com.codefest.codefest.service"})

@SpringBootApplication
public class CodefestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodefestApplication.class, args);
	}
}
