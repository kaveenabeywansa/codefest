package com.codefest.codefest.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.codefest.codefest.model.EmpStep;
import com.codefest.codefest.model.EmpStepRepository;
import com.codefest.codefest.model.Step;
import com.codefest.codefest.model.StepRepository;

@Service
public class StepServiceImpl {

	@Autowired
	private StepRepository StepRepository;
	
	public Step addStep( Step instance) {
		try {
			return StepRepository.save(instance);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public List<Step> getStepDetails() {
		List<Step> StepDetails = null;
		try {
			StepDetails = StepRepository.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			return StepDetails;
		}
	}
	
	public Step FindOne(String Id) {
		Step instance = null;
		try {
			List<Step> StepList = StepRepository.findAll();
			for (Step i : StepList) {
				if (i.getDate().equals(Id)) {
					instance = i;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return instance;
		}
	}
	
	public Step getStepDetailsById( String Id) {
		Step instance = null;
		try {
			instance = FindOne(Id);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return instance;
		}
	}
	
	public void deleteStepDetails( String Id) {
		try {
			Step instance = FindOne(Id);
			if (instance != null)
				StepRepository.delete(instance);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Step FindOneByStepId(String Id) {
		Step instance = null;
		try {
			List<Step> StepList = StepRepository.findAll();
			for (Step i : StepList) {
				if (i.getStepid().equals(Id)) {
					instance = i;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return instance;
		}
	}
	
	
	
}
