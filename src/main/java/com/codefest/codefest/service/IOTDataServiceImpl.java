package com.codefest.codefest.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codefest.codefest.model.BeltPerSec;
import com.codefest.codefest.model.BeltRepository;
import com.codefest.codefest.model.IOTData;
import com.codefest.codefest.model.IOTRepository;

@Service
public class IOTDataServiceImpl {
	@Autowired
	private IOTRepository iotrepo;

	public void addCycle(IOTData instance) {
		try {
			iotrepo.save(instance);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<IOTData> getIOTDetails() {
		List<IOTData> IOTDet = null;
		try {
			IOTDet = iotrepo.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return IOTDet;
		}
	}

	public IOTData getDeviceDetails(String Id) {
		IOTData instance = null;
		try {
			instance = FindOne(Id);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return instance;
		}
	}

	public void deleteIOTDet(String Id) {
		try {
			IOTData instance = FindOne(Id);
			if (instance != null)
				iotrepo.delete(instance);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public IOTData FindOne(String Id) {
		IOTData instance = null;
		try {
			List<IOTData> beltList = iotrepo.findAll();
			for (IOTData i : beltList) {
				if (i.getIotid().equals(Id)) {
					instance = i;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return instance;
		}
	}
}
