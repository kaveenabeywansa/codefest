//package com.codefest.codefest.service;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.codefest.codefest.model.BeltPerSec;
//import com.codefest.codefest.model.BeltRepository;
//
//@RestController
//@RequestMapping("/iot")
//public class BeltResource {
//	@Autowired
//	private BeltRepository beltRepo;
//	
//	@RequestMapping(method = RequestMethod.POST)
//	public void addCycle(@RequestBody BeltPerSec instance) {
//		try {
//			beltRepo.save(instance);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//	
//	@SuppressWarnings("finally")
//	@RequestMapping(method = RequestMethod.GET)
//	public List<BeltPerSec> getIOTDetails() {
//		List<BeltPerSec> IOTDet = null;
//		try {
//			IOTDet = beltRepo.findAll();
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			return IOTDet;
//		}
//	}
//	
//	@SuppressWarnings("finally")
//	@RequestMapping(value = "/{Id}", method = RequestMethod.GET)
//	public BeltPerSec getDeviceDetails(@PathVariable String Id) {
//		BeltPerSec instance = null;
//		try {
//			instance = FindOne(Id);
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			return instance;
//		}
//	}
//	
//	@RequestMapping(value = "/{Id}", method = RequestMethod.DELETE)
//	public void deleteIOTDet(@PathVariable String Id) {
//		try {
//			BeltPerSec instance = FindOne(Id);
//			if (instance != null)
//				beltRepo.delete(instance);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//	
//	@SuppressWarnings("finally")
//	public BeltPerSec FindOne(String Id) {
//		BeltPerSec instance = null;
//		try {
//			List<BeltPerSec> beltList = beltRepo.findAll();
//			for (BeltPerSec i : beltList) {
//				if (i.getIotcode().equals(Id)) {
//					instance = i;
//					break;
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			return instance;
//		}
//	}
//}
