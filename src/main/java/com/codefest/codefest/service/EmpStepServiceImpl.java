package com.codefest.codefest.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.codefest.codefest.model.EmpStep;
import com.codefest.codefest.model.EmpStepRepository;
import com.codefest.codefest.model.Step;
import com.codefest.codefest.model.StepRepository;

@Service
public class EmpStepServiceImpl {

	@Autowired
	private EmpStepRepository empStepRepository;

	public EmpStep addEmpStep(EmpStep instance) {
		try {
			return empStepRepository.save(instance);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<EmpStep> getEmpStepDetails() {
		List<EmpStep> EmpStepDetails = null;
		try {
			EmpStepDetails = empStepRepository.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			return EmpStepDetails;
		}
	}

	public EmpStep getEmpStepDeatailsbyNic(String Id) {
		EmpStep instance = null;
		try {
			List<EmpStep> EmpStepList = empStepRepository.findAll();
			for (EmpStep i : EmpStepList) {
				if (i.getNic().equals(Id)) {
					instance = i;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			return instance;
		}
	}

	public EmpStep getEmpStepDetailsById(String Id) {
		EmpStep instance = null;
		try {
			instance = getEmpStepDeatailsbyNic(Id);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			return instance;
		}
	}

	public void deleteEmpStepDetails(String Id) {
		try {
			EmpStep instance = getEmpStepDeatailsbyNic(Id);
			if (instance != null)
				empStepRepository.delete(instance);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<EmpStep> FindStepAssignments(String StepId) {
		List<EmpStep> instance = new ArrayList<>();
		try {
			List<EmpStep> EmpStepList = empStepRepository.findAll();
			for (EmpStep i : EmpStepList) {
				if (i.getStepid().equals(StepId)) {
					instance.add(i);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			return instance;
		}
	}

	// update the employee's step id by nic when swapping steps
	public void updateEmpStepDetails(String nic, EmpStep paraInstance) {
		try {
			this.deleteEmpStepDetails(nic);
			empStepRepository.save(paraInstance);
//			EmpStep instance = getEmpStepDeatailsbyNic(nic);
//			if (instance != null) {
//				instance = paraInstance;
////				empStepRepository.u(instance);
////				instance.setStepid(stepId);
//				empStepRepository.save(instance);
//			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getAssignedEmpCount(String stepId) {
		int count = 0;
		try {
			List<EmpStep> EmpStepList = empStepRepository.findAll();
			for (EmpStep i : EmpStepList) {
				if (i.getStepid().equals(stepId)) {
					count++;
				}
			}

//			Step instance = FindOneByStepId(stepId);
//			StepRepository.delete(instance);
//			StepRepository.save(inst);

			// call a performance update method

//			if (instance != null)
//				StepRepository.save(instance);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return count;
		}
	}

}
