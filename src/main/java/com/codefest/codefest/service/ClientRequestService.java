package com.codefest.codefest.service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.codefest.codefest.model.ClientRequestModel;
import com.codefest.codefest.model.ClientRequestRepository;
import com.codefest.codefest.model.IOTData;
import com.codefest.codefest.model.IOTRepository;
import com.codefest.codefest.model.Step;
import com.codefest.codefest.model.StepRepository;

@Service
public class ClientRequestService {
	@Autowired
	private IOTDataServiceImpl iotDataServiceImpl;
	@Autowired
	private EmpStepServiceImpl empStepServiceImpl;
	@Autowired
	private ClientRequestRepository clientRequestRepository;

//	public List<ClientRequestModel> getAllData() {
//		List<ClientRequestModel> clientData = null;
//		try {
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return clientData;
//	}
	
	
	//
	public double getDeviceErrorRate(String Id) {
		double errorRate = 0;
		try {
			double defCount = (double) iotDataServiceImpl.getDeviceDetails(Id).getDefectcount();
			double totCount = (double) iotDataServiceImpl.getDeviceDetails(Id).getTotcount();
			errorRate = (defCount/totCount)*100;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return errorRate;
	}
	public double getDevicePerformance(String StepId) {
		double performance=0;
		try {
			int empCount = empStepServiceImpl.getAssignedEmpCount(StepId);
			double errorRate = getDeviceErrorRate(StepId);
			performance = (100-(errorRate/empCount));
			
			ClientRequestModel instance = new ClientRequestModel();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			instance.setDate(sdf.format(new Date()));
			instance.setEmpPerformance(performance);
			instance.setErrorRate(errorRate);
			instance.setIotId(StepId);
			clientRequestRepository.save(instance);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return performance;
	}
	
	//
	
	
	
//	public void generateData() {
//		try {
//			LocalDateTime now = LocalDateTime.now();
//			List<IOTData> iotInstance = iotDataServiceImpl.getIOTDetails();
//			List<Step> stepInstance = stepServiceImpl.getStepDetails();
//			List<ClientRequestModel> generatedData = null; // Returning data list
////			List<EmpStepRepository> empStepInstance =
//			for(int i = 0; i<iotInstance.size();i++) {
//				IOTData iotDevice = iotInstance.get(i);
//				Step step = stepInstance.get(i);
//				
//				ClientRequestModel genDataInstance = null;
//				double errorRate = ((iotDevice.getDefectcount()/iotDevice.getTotcount())*100);
//				double empPerformance = 100 - ((errorRate/step.getEmpcount())*100); 
//				genDataInstance.setErrorRate(errorRate);
//				genDataInstance.setEmpPerformance(empPerformance);
//				generatedData.add(genDataInstance);
//				genDataInstance = null;
//			}
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

//	public List<IOTData> getIOTDetails() {
//		List<IOTData> IOTDet = null;
//		try {
//			IOTDet = iotrepo.findAll();
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			return IOTDet;
//		}
//	}
//
//	public IOTData getDeviceDetails(String Id) {
//		IOTData instance = null;
//		try {
//			instance = FindOne(Id);
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			return instance;
//		}
//	}
//
//	public void deleteIOTDet(String Id) {
//		try {
//			IOTData instance = FindOne(Id);
//			if (instance != null)
//				iotrepo.delete(instance);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	public IOTData FindOne(String Id) {
//		IOTData instance = null;
//		try {
//			List<IOTData> beltList = iotrepo.findAll();
//			for (IOTData i : beltList) {
//				if (i.getDate().equals(Id)) {
//					instance = i;
//					break;
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			return instance;
//		}
//	}
}
